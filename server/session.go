package main

import (
	"gitlab.com/kohlten/packet_parser"
	"math/rand"
	"time"
)

const (
	sessionMaxClients = 2
	sessionMinClients = 2
)

type Session struct {
	cycles  []*Cycle
	clients []*Client
	id      int
	paused  bool
}

func shuffle(vals []*Client) {
	r := rand.New(rand.NewSource(time.Now().Unix()))
	for len(vals) > 0 {
		n := len(vals)
		randIndex := r.Intn(n)
		vals[n-1], vals[randIndex] = vals[randIndex], vals[n-1]
		vals = vals[:n-1]
	}
}

func NewSession(server *Server, clients []*Client) *Session {
	shuffle(clients)
	session := new(Session)
	for i := 0; i < len(clients); i++ {
		session.cycles = append(session.cycles, NewCycle(Vector2f{X: windowSizeX, Y: windowSizeY}, i))
		client := clients[i]
		server.server.SendPacket(client.address, packet_parser.FormatPacket(Identifier, i))
		server.server.SendPacket(client.address, packet_parser.FormatPacket(NumCycles, len(clients)))
	}
	session.clients = clients
	for i := 0; i < len(clients); i++ {
		client := clients[i]
		server.server.SendPacket(client.address, packet_parser.FormatPacket(SessionStart))

	}
	return session
}

func (session *Session) checkForCollisions(server *Server) {
	for i := 0; i < len(session.clients); i++ {
		cyclePos := session.cycles[i].GetPosition()
		for j := 0; j < len(session.clients); j++ {
			for k := 0; k < session.cycles[j].GetPreviousPositionsLength(); k++ {
				otherCyclePos := session.cycles[j].GetPreviousPosition(k)
				if cyclePos.X == otherCyclePos.X && cyclePos.Y == otherCyclePos.Y {
					session.cycles[i].SetDied(true)
					logPrint("Sent died packet to client ", session.clients[i].address)
					server.server.SendPacket(session.clients[i].address, packet_parser.FormatPacket(Died))
				}
			}
		}
	}
}

func (session *Session) Update(server *Server) {
	if !session.paused {
		for i := 0; i < len(session.cycles); i++ {
			session.cycles[i].Update(Vector2f{X: windowSizeX, Y: windowSizeY})
		}
		for i := 0; i < len(session.clients); i++ {
			if !session.cycles[i].IsDead() {
				client := session.clients[i]
				for j := 0; j < len(session.cycles); j++ {
					sessionCycle := session.cycles[j]
					position := sessionCycle.GetPosition()
					server.server.SendPacket(client.address, packet_parser.FormatPacket(Position, j, position.X, position.Y))
				}
			}
		}
		session.checkForCollisions(server)
	}
}

func (session *Session) RemoveClient(cycleLocation int, clients []*Client) {
	session.clients = append(session.clients[:cycleLocation], session.clients[cycleLocation+1:]...)
	session.cycles[cycleLocation].SetDied(true)
}

func (session *Session) IsDone() bool {
	cyclesDead := 0
	for i := 0; i < len(session.cycles); i++ {
		if session.cycles[i].IsDead() {
			cyclesDead++
		}
	}
	return len(session.cycles)-1 == cyclesDead || cyclesDead == len(session.cycles)
}

func (session *Session) Pause() {
	session.paused = !session.paused
}
