package main

import (
	"gitlab.com/kohlten/packet_parser"
	"fmt"
	"strconv"
	"time"
)

const (
	PacketUnknownAction = "unknown action"
)

const (
	// Session Actions
	AddToQueue      = 0x82
	RemoveFromQueue = 0x83
	SessionStart    = 0x84
	SessionEnd      = 0x85
	NumCycles       = 0x87
	Pause           = 0x8c


	// Cycle Actions
	Position   = 0x88
	Direction  = 0x89
	Died       = 0x8a
	Identifier = 0x8b

	// General Actions
	Ping = 0x71
	Disconnect = 0x72
)


func ping(packet *packet_parser.PacketData, a ...interface{}) {
	server := a[0].(*Server)
	server.checkClient(packet.Addr)
	clientIndex := server.findClientFromAddress(packet.Addr)
	if clientIndex >= 0 {
		client := server.clients[clientIndex]
		client.lastPing = uint64(time.Now().UnixNano() / int64(time.Millisecond))
		server.server.SendPacket(client.address, packet_parser.FormatPacket(Ping))
	}
}

func disconnect(packet *packet_parser.PacketData, a ...interface{}) {
	server := a[0].(*Server)
	server.checkClient(packet.Addr)
	clientIndex := server.findClientFromAddress(packet.Addr)
	if clientIndex >= 0 {
		client := server.clients[clientIndex]
		logPrint("Removed client", client.address)
		server.RemoveClient(client)
	}
}

func changeCycleDirection(packet *packet_parser.PacketData, a ...interface{}) {
	server := a[0].(*Server)
	server.checkClient(packet.Addr)
	clientIndex := server.findClientFromAddress(packet.Addr)
	if clientIndex >= 0 {
		client := server.clients[clientIndex]
		if len(packet.Data) < 2 {
			return
		}
		cycleId, _ := strconv.Atoi(string(packet.Data[0]))
		newDir, _ := strconv.Atoi(string(packet.Data[1]))
		i, j := server.FindClientSession(client)
		if i == -1 && j == -1 {
			return
		}
		server.sessions[i].cycles[cycleId].AddDir(uint32(newDir))
	}
}

func addToQueue(packet *packet_parser.PacketData, a ...interface{}) {
	server := a[0].(*Server)
	server.checkClient(packet.Addr)
	clientIndex := server.findClientFromAddress(packet.Addr)
	if clientIndex >= 0 {
		client := server.clients[clientIndex]
		server.AddClientToQueue(client)
	}
}

func removeFromQueue(packet *packet_parser.PacketData, a ...interface{}) {
	server := a[0].(*Server)
	server.checkClient(packet.Addr)
	clientIndex := server.findClientFromAddress(packet.Addr)
	if clientIndex >= 0 {
		client := server.clients[clientIndex]
		server.RemoveClientFromQueue(client)
	}
}

func pauseSession(packet *packet_parser.PacketData, a...interface{}) {
	server := a[0].(*Server)
	fmt.Println("Pausing")
	server.checkClient(packet.Addr)
	clientIndex := server.findClientFromAddress(packet.Addr)
	if clientIndex >= 0 {
		client := server.clients[clientIndex]
		i, j := server.FindClientSession(client)
		if i == -1 && j == -1 {
			return
		}
		server.sessions[i].Pause()
	}
}