package main

import (
	"net"
	"time"
)

const (
	ClientTimeoutTime = 4 * 1000
	ClientRemoveTime = 1000 * 60
)

const (
	ClientActive   = 0
	ClientTimedOut = 1
)

type Client struct {
	address *net.UDPAddr
	lastPing uint64
	state int32
}

func NewClient(address *net.UDPAddr) *Client{
	client := new(Client)
	client.lastPing = uint64(time.Now().UnixNano() / int64(time.Millisecond))
	client.address = address
	return client
}

func (client *Client) SwitchState(state int32) {
	if client.state != state {
		client.state = state
	}
}
