package main

import (
	"errors"
	"fmt"
	"gitlab.com/kohlten/gamelib/db"
	"log"
	"os"
	"os/signal"
	"runtime/debug"
)

const (
	configName = "server.config"
)

var logger *log.Logger
var logfile *os.File
var config *db.Database

func createConfig() {
	var err error
	config, err = db.NewDatabase(configName, "COPY", db.DefaultMaxNodesPerPage)
	if err != nil {
		panic(err)
	}
	_ = config.SetValue("logfile", "server.log")
	_ = config.SetValue("port", 8181)
	err = config.Dump()
	if err != nil {
		panic(err)
	}
}

func init() {
	debug.SetGCPercent(11300)
	if _, err := os.Stat(configName); err == nil {
		config, err = db.LoadDatabase(configName)
		if err != nil {
			panic(fmt.Sprint("Failed to open config file ", configName, " with error ", err.Error()))
		}
	} else {
		createConfig()
	}
	err := errors.New("")
	logfileNameTmp, _, _ := config.GetValue("logfile")
	logfileName := logfileNameTmp.(string)
	logfile, err = os.OpenFile(logfileName, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	if err != nil {
		panic(fmt.Sprint("Failed to open logfile with error ", err.Error()))
	}
	logger = log.New(logfile, "", log.Ldate|log.Ltime|log.Llongfile)
}

func signalHandler(signals chan os.Signal, gameRunning *bool) {
	signalCaught := <-signals
	*gameRunning = false
	_ = config.Dump()
	logger.Fatalln("Crashed with signal", signalCaught)
}

func main() {
	port, _, _ := config.GetValue("port")
	server, err := NewServer(port.(int))
	if err != nil {
		panic("Failed to create a server!")
	}
	signals := make(chan os.Signal, 1)
	signal.Notify(signals)
	go signalHandler(signals, &server.running)
	server.Run()
	server.Free()
	_ = logfile.Close()
	_ = config.Dump()
}
