package main

import (
	"fmt"
	"gitlab.com/kohlten/game_server/callbacks"
	gameServer "gitlab.com/kohlten/game_server/server"
	"gitlab.com/kohlten/packet_parser"
	"net"
	"time"
)

const (
	windowSizeX = 400
	windowSizeY = 400

	maxWaitTime = 5 * 1000
)

type Server struct {
	server            *gameServer.GameServer
	clients           []*Client
	queue             []int
	sessions          []*Session
	enoughPlayersTime uint64
	running           bool
	currentClient     *Client
}

func logPrint(a ...interface{}) {
	logger.Println(a...)
	fmt.Println(a...)
}

func NewServer(port int) (*Server, error) {
	server := new(Server)
	s, err := gameServer.NewGameServerCallbacks(uint16(port), 1, packet_parser.FormatPacket, packet_parser.ParsePacket)
	if err != nil {
		return nil, err
	}
	server.server = s
	server.server.AddCallback(Ping, ping, server)
	server.server.AddCallback(Disconnect, disconnect, server)
	server.server.AddCallback(Direction, changeCycleDirection, server)
	server.server.AddCallback(AddToQueue, addToQueue, server)
	server.server.AddCallback(RemoveFromQueue, removeFromQueue, server)
	server.server.AddCallback(Pause, pauseSession, server)
	server.server.AddCallback(callbacks.UnusedPacket, callbacks.NilCallback, server)
	server.running = true
	logPrint("New server on address", server.server.GetAddress())
	return server, nil
}

func (server *Server) createNewSession() {
	clientsLen := 0
	if len(server.queue) >= sessionMaxClients {
		clientsLen = sessionMaxClients
	} else {
		clientsLen = len(server.queue)
	}
	clients := server.queue[:clientsLen]
	server.queue = server.queue[clientsLen:]
	clientSocks := make([]*Client, len(clients))
	for i := 0; i < len(clients); i++ {
		clientSocks[i] = server.clients[clients[i]]
	}
	session := NewSession(server, clientSocks)
	server.sessions = append(server.sessions, session)
	clientIps := ""
	for i := 0; i < len(clients); i++ {
		clientIps += server.clients[clients[i]].address.String() + " "
	}
	logPrint("Created new session", server.sessions[len(server.sessions)-1].id, "with clients", clientIps)
}

func (server *Server) updateSessions() {
	currentTime := uint64(time.Now().UnixNano() / int64(time.Millisecond))
	if len(server.queue) >= sessionMaxClients || (currentTime-server.enoughPlayersTime >= maxWaitTime && len(server.queue) >= sessionMinClients) {
		server.createNewSession()
	}
	for i := 0; i < len(server.sessions); {
		server.sessions[i].Update(server)
		if server.sessions[i].IsDone() {
			for j := 0; j < len(server.sessions[i].clients); j++ {
				client := server.sessions[i].clients[j]
				packet := packet_parser.FormatPacket(SessionEnd)
				server.server.SendPacket(client.address, packet)
			}
			logPrint("Session", i, "finished.")
			server.sessions = append(server.sessions[:i], server.sessions[i+1:]...)
		} else {
			i++
		}
	}
}

func (server *Server) checkClient(addr *net.UDPAddr) *Client {
	index := server.findClientFromAddress(addr)
	var client *Client
	if index < 0 {
		client = NewClient(addr)
		server.clients = append(server.clients, client)
		logPrint("New client on address", client.address.String())
	} else {
		client = server.clients[index]
	}
	if client.state == ClientTimedOut {
		fmt.Println("Client ", client.address.String(), " became active again.")
		logger.Println("Client ", client.address.String(), " became active again.")
		client.SwitchState(ClientActive)
	}
	return client
}

func (server *Server) updateClients() {
	for i := 0; i < len(server.clients); {
		client := server.clients[i]
		currentTime := uint64(time.Now().UnixNano() / int64(time.Millisecond))
		if client.state == ClientActive && currentTime-client.lastPing > uint64(ClientTimeoutTime) {
			client.SwitchState(ClientTimedOut)
			logPrint("Client", client.address.String(), "timed out")
		}
		if client.state == ClientTimedOut && currentTime-client.lastPing > uint64(ClientRemoveTime) {
			logPrint("Client", client.address.String(), "was removed due to time out")
			server.RemoveClient(client)
		} else {
			i++
		}
	}
}

func (server *Server) Run() {
	for server.running {
		start := int(time.Now().UnixNano() / int64(time.Millisecond))
		server.updateClients()
		server.updateSessions()
		end := int(time.Now().UnixNano() / int64(time.Millisecond))
		if end-start < 5 {
			time.Sleep(time.Duration(16-(end-start)) * time.Millisecond)
		}
	}
}

func (server *Server) findClient(client *Client) int {
	for i, serverClient := range server.clients {
		if serverClient.lastPing == client.lastPing &&
			serverClient.state == client.state &&
			serverClient.address.String() == client.address.String() {
			return i
		}
	}
	return -1
}

func (server *Server) findClientFromAddress(addr *net.UDPAddr) int {
	strAddr := addr.String()
	for i, serverClient := range server.clients {
		if strAddr == serverClient.address.String() {
			return i
		}
	}
	return -1
}

func (server *Server) ClientInQueue(client *Client) bool {
	found := false
	clientNum := server.findClient(client)
	for i := 0; i < len(server.queue); i++ {
		if server.queue[i] == clientNum {
			found = true
			break
		}
	}
	return found
}

func (server *Server) ClientInSession(client *Client) bool {
	i, j := server.FindClientSession(client)
	return !(i == -1 && j == -1)
}

func (server *Server) AddClientToQueue(client *Client) {
	if !server.ClientInQueue(client) && !server.ClientInSession(client) {
		server.queue = append(server.queue, server.findClient(client))
		if len(server.queue) >= sessionMinClients {
			server.enoughPlayersTime = uint64(time.Now().UnixNano() / int64(time.Millisecond))
		}
		logPrint("Added client ", client.address.String(), " to queue")
	}
}

func (server *Server) RemoveClientFromQueue(client *Client) {
	if server.ClientInQueue(client) {
		clientNum := server.findClient(client)
		i := 0
		for ; i < len(server.queue); i++ {
			if server.queue[i] == clientNum {
				break
			}
		}
		server.queue = append(server.queue[:i], server.queue[i+1:]...)
		if len(server.queue) < sessionMinClients && server.enoughPlayersTime > 0 {
			server.enoughPlayersTime = 0
		}
		logPrint("Removed client ", client.address.String(), " from queue")
	}
}

func (server *Server) FindClientSession(client *Client) (int, int) {
	location := server.findClient(client)
	for i := 0; i < len(server.sessions); i++ {
		for j := 0; j < len(server.sessions[i].clients); j++ {
			sessionClientLocation := server.findClient(server.sessions[i].clients[j])
			if sessionClientLocation == location {
				return i, j
			}
		}
	}
	return -1, -1
}

func (server *Server) RemoveClientSession(client *Client) {
	sessionLocation, sessionCycle := server.FindClientSession(client)
	if sessionLocation >= 0 && sessionCycle >= 0 {
		server.sessions[sessionLocation].RemoveClient(sessionCycle, server.clients)
	}
}

func (server *Server) RemoveClient(client *Client) {
	if server.ClientInQueue(client) {
		server.RemoveClientFromQueue(client)
	}
	i, j := server.FindClientSession(client)
	if i > -1 && j > -1 {
		server.RemoveClientSession(client)
	}
	location := server.findClient(client)
	server.clients = append(server.clients[:location], server.clients[location+1:]...)
}

func (server *Server) Free() {
	server.running = false
	server.server.Close()
}
