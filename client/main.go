package main

import (
	"fmt"
	"gitlab.com/kohlten/game_server"
	"gitlab.com/kohlten/gamelib"
	"gitlab.com/kohlten/packet_parser"
	"os"
	"os/signal"
	"runtime"
	"time"
)

/*
TODO:
	* Make a better way of installing
	* Multiple clients in the lobby
	* Check what is taking so much time on startup
	* Add the assets to an archive
	* Add options in the main menu
	* Add better graphics for the cycles and menu
	* Clean up scene code
	* Fix issue with being able to control other cycles by sending their id
*/

const (
	configFile = "client.config"
)

var config *gamelib.Config
var windowSize []float64
var handshakeWaitTime float64

func createConfig() {
	config = gamelib.NewBlankConfig()
	_ = config.SetDataString("serverAddress", "35.88.56.170")
	_ = config.SetDataNum("serverPort", 8181)
	_ = config.SetDataNum("handshakeWaitTime", 4000)
	_ = config.SetDataVector("windowSize", []float64{400, 400})
	_ = config.SetDataVector("snakeOneColor", []float64{255, 0, 0, 255})
	_ = config.SetDataVector("snakeTwoColor", []float64{0, 255, 0, 255})
	_ = config.SetDataVector("snakeThreeColor", []float64{0, 0, 255, 255})
	_ = config.SetDataVector("snakeFourColor", []float64{255, 255, 0, 255})
	_ = config.SetDataVector("snakeHeadColor", []float64{0, 155, 255, 255})
	_ = config.SetDataBool("showFPS", true)
	_ = config.SetDataBool("showPing", true)
	_ = config.WriteToFile(configFile)
}

func init() {
	runtime.LockOSThread()
	if _, err := os.Stat(configFile); err == nil {
		config, err = gamelib.ParseConfig(configFile)
		if err != nil {
			panic(fmt.Sprint("Failed to open config file with error ", err.Error()))
		}
	} else {
		createConfig()
	}
	handshakeWaitTime, _ = config.GetDataNum("handshakeWaitTime")
	windowSize, _ = config.GetDataVector("windowSize")
}

func signalHandler(client *Client, signals chan os.Signal, gameRunning *bool) {
	<-signals
	sendDisconnect(client)
	_ = config.WriteToFile(configFile)
	*gameRunning = false
}

func handshake(client *Client) {
	start := game_server.GetTimeMS()
	pinged := false
	pingFunc := func(packet *packet_parser.PacketData, a ...interface{}) {
		pinged = true
	}
	sendPing(client)
	client.sock.AddCallback(Ping, pingFunc)
	for {
		if pinged {
			return
		}
		currentTime := game_server.GetTimeMS()
		if currentTime-start > uint64(handshakeWaitTime) {
			panic("Failed to connect to server")
		}
		time.Sleep(time.Millisecond * 16)
	}
}

func main() {
	address, _ := config.GetDataString("serverAddress")
	port, _ := config.GetDataNum("serverPort")
	client, err := NewClient(address, uint16(port))
	if err != nil {
		panic(err.Error())
	}
	handshake(client)
	game, succeed := NewGame(client)
	if !succeed {
		if game != nil {
			sendDisconnect(game.client)
		}
		os.Exit(1)
	}
	signals := make(chan os.Signal, 1)
	signal.Notify(signals)
	go signalHandler(game.client, signals, &game.running)
	game.Run()
	game.Free()
	_ = config.WriteToFile(configFile)
}
