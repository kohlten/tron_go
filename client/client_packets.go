package main

import (
	"gitlab.com/kohlten/packet_parser"
)

const (
	// Session Actions
	AddToQueue      = 0x82
	RemoveFromQueue = 0x83
	SessionStart    = 0x84
	SessionEnd      = 0x85
	NumCycles       = 0x87
	Pause           = 0x8c

	// Cycle Actions
	Position   = 0x88
	Direction  = 0x89
	Died       = 0x8a
	Identifier = 0x8b

	// General Actions
	Ping = 0x71
	Disconnect = 0x72
)

func sendJoinQueue(client *Client) {
	packet := packet_parser.FormatPacket(AddToQueue)
	client.sock.SendPacket(packet)
}

func sendRemoveFromQueue(client *Client) {
	packet := packet_parser.FormatPacket(RemoveFromQueue)
	client.sock.SendPacket(packet)
}

func sendDirection(client *Client, cycle, direction int) {
	packet := packet_parser.FormatPacket(Direction, cycle, direction)
	client.sock.SendPacket(packet)
}

func sendDisconnect(client *Client) {
	packet := packet_parser.FormatPacket(Disconnect)
	client.sock.SendPacket(packet)
}

func sendPing(client *Client) {
	packet := packet_parser.FormatPacket(Ping)
	client.sock.SendPacket(packet)
}

func sendPause(client *Client) {
	packet := packet_parser.FormatPacket(Pause)
	client.sock.SendPacket(packet)
}
