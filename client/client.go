package main

import (
	gameClient "gitlab.com/kohlten/game_server/client"
	"gitlab.com/kohlten/packet_parser"
	"time"
)

const (
	PingPeriod = 1000
)

type Client struct {
	sock           *gameClient.GameClient
	lastPing       uint64
	serverPingTime uint64
}

func NewClient(address string, port uint16) (*Client, error) {
	client := new(Client)
	sock, err := gameClient.NewGameClientCallbacks(address, port, 1, packet_parser.FormatPacket, packet_parser.ParsePacket)
	if err != nil {
		return nil, err
	}
	client.sock = sock
	client.lastPing = uint64(time.Now().UnixNano() / int64(time.Millisecond))
	return client, nil
}

func (client *Client) Update() {
	if uint64(time.Now().UnixNano()/int64(time.Millisecond))-client.lastPing >= PingPeriod {
		sendPing(client)
		client.lastPing = uint64(time.Now().UnixNano() / int64(time.Millisecond))
	}
}

func (client *Client) Free() {
	client.sock.Close()
}
