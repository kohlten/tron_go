package main

import (
	"gitlab.com/kohlten/gamelib"
	"gitlab.com/kohlten/gamelib/vector"

	"strconv"
	// "gitlab.com/kohlten/tron_go/packet_parser"
)

const (
	MenuStateNone = 0
	MenuStateQuit = 1
)

type MenuScene struct {
	text          *gamelib.Text
	playButton    *gamelib.Button
	optionsButton *gamelib.Button
	quitButton    *gamelib.Button
	queueTimer    *gamelib.Timer
	inQueue       bool
}

func NewMenuScene(window *gamelib.Window) (*MenuScene, error) {
	menu := new(MenuScene)
	text, err := gamelib.NewTextFromJson("assets/fasttracker2-style_12x12.json", window.Renderer)
	if err != nil {
		return nil, err
	}
	menu.text = text
	buttonRegSurf, err := gamelib.NewTextureFromFile("assets/button.png", window.Renderer)
	if err != nil {
		return nil, err
	}
	buttonHoverSurf, err := gamelib.NewTextureFromFile("assets/button_hover.png", window.Renderer)
	if err != nil {
		return nil, err
	}
	menu.queueTimer = gamelib.NewTimer(gamelib.TimerInfinite, gamelib.TimeNone)
	buttonSize := buttonRegSurf.Size
	buttonPos := vector.NewVec2(float32(windowSize[0])/2, float32(windowSize[1])/2)
	buttonSpacing := buttonSize.Y / 2
	menu.playButton = gamelib.NewButton(menu.text, buttonPos,
		buttonSize, 1.0,
		buttonRegSurf, buttonHoverSurf, nil)
	menu.playButton.SetString("Play")
	menu.optionsButton = gamelib.NewButton(menu.text, vector.NewVec2(buttonPos.X, buttonPos.Y+buttonSize.Y+buttonSpacing),
		buttonSize, 1.0,
		buttonRegSurf, buttonHoverSurf, nil)
	menu.optionsButton.SetString("Options")
	menu.quitButton = gamelib.NewButton(menu.text, vector.NewVec2(buttonPos.X,
		buttonPos.Y+(buttonSize.Y*2)+(buttonSpacing*2)),
		buttonSize, 1.0,
		buttonRegSurf, buttonHoverSurf, nil)
	menu.quitButton.SetString("Quit")
	return menu, nil
}

func (menu *MenuScene) Init(data ...interface{}) {

}

func (menu *MenuScene) Update(userData interface{}) (interface{}, error) {
	client := userData.(*Client)
	menu.playButton.Update()
	menu.optionsButton.Update()
	menu.quitButton.Update()
	if menu.playButton.IsPressed() && !menu.inQueue {
		sendJoinQueue(client)
		menu.playButton.SetString("Leave Queue")
		menu.inQueue = true
		menu.playButton.SetClicked(false)
		menu.queueTimer.Start()
	} else if menu.playButton.IsPressed() && menu.inQueue {
		sendRemoveFromQueue(client)
		menu.playButton.SetString("Play")
		menu.inQueue = false
		menu.playButton.SetClicked(false)
		menu.queueTimer.Stop()
	}
	if menu.quitButton.IsPressed() {
		return MenuStateQuit, nil
	}
	return MenuStateNone, nil
}

func (menu *MenuScene) ParseEvent(event, userData interface{}) {
	// No Events
}

func (menu *MenuScene) Draw(window_interface interface{}) error {
	window := window_interface.(*gamelib.Window)
	renderer := window.Renderer
	titleSize := menu.text.GetSize("Tron GO", 1.0)
	titlePos := vector.NewVec2(float32(windowSize[0])/2-titleSize.X/2, float32(windowSize[1])/3-titleSize.Y/2)
	err := menu.text.Draw("Tron GO", titlePos, vector.Zero, 0.0, 1.0, nil, renderer)
	if err != nil {
		return err
	}
	err = menu.playButton.Draw(0, renderer)
	if err != nil {
		return err
	}
	if !menu.inQueue {
		err = menu.optionsButton.Draw(0, renderer)
		if err != nil {
			return err
		}
		err = menu.quitButton.Draw(0, renderer)
		if err != nil {
			return err
		}
	} else {
		currentTime := menu.queueTimer.GetElapsedTime(gamelib.TimeMilliseconds) / 1000
		minutes := uint64(0)
		seconds := uint64(0)
		if currentTime > 0 {
			minutes = currentTime / 60
			seconds = currentTime % 60
		}
		textMinutes := strconv.FormatUint(minutes, 10)
		textSeconds := strconv.FormatUint(seconds, 10)
		if minutes < 10 {
			textMinutes = "0" + textMinutes
		}
		if seconds < 10 {
			textSeconds = "0" + textSeconds
		}
		text := textMinutes + ":" + textSeconds
		textSize := menu.text.GetSize(text, 1.0)
		position := vector.NewVec2(float32(windowSize[0])/2-textSize.X/2, 5)
		err = menu.text.Draw(text, position, vector.Zero, 0.0, 1.0, nil, renderer)
		if err != nil {
			return err
		}
	}
	return nil
}

func (menu *MenuScene) Reset() {
	menu.playButton.SetString("Play")
	menu.playButton.Reset()
	menu.optionsButton.Reset()
	menu.quitButton.Reset()
	menu.queueTimer.Stop()
	menu.inQueue = false
}

func (menu *MenuScene) GetName() string {
	return "menu"
}

func (menu *MenuScene) Free() {
	menu.text.Free()
	menu.playButton.Free()
}
