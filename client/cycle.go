package main

import (
	"github.com/veandco/go-sdl2/sdl"
	"golang.org/x/image/math/f32"
)

type Vector2f struct {
	X, Y float64
}

const (
	PosTop         = 0
	PosBottom      = 1
	PosTopRight    = 2
	PosBottomRight = 3

	DirUp    = 4
	DirDown  = 5
	DirLeft  = 6
	DirRight = 7

	Speed     = 1.0
	TotalSize = 6.0
	Size      = 5.0
)

type Cycle struct {
	position          Vector2f
	previousPositions []Vector2f
	dirQueue          []uint32
	dir               uint32
	died              bool
}

func NewCycle(windowSize Vector2f, startPosition int) *Cycle {
	cycle := new(Cycle)
	switch startPosition {
	case PosTop:
		cycle.position = Vector2f{0, 0}
		cycle.dir = DirRight
	case PosBottom:
		cycle.position = Vector2f{0, windowSize.Y}
		cycle.dir = DirUp
	case PosTopRight:
		cycle.position = Vector2f{windowSize.X, 0}
		cycle.dir = DirDown
	case PosBottomRight:
		cycle.position = Vector2f{windowSize.X, windowSize.Y}
		cycle.dir = DirLeft
	}
	return cycle
}

func (cycle *Cycle) Update(windowSize Vector2f) {
	if !cycle.died {
		if len(cycle.dirQueue) > 0 && int(cycle.position.X) % TotalSize == 0 && int(cycle.position.Y) % TotalSize == 0 {
			cycle.setDir(cycle.dirQueue[0])
			cycle.dirQueue = cycle.dirQueue[1:]
		}
		cycle.previousPositions = append(cycle.previousPositions, cycle.position)
		switch cycle.dir {
		case DirDown:
			cycle.position.Y += Speed
		case DirUp:
			cycle.position.Y -= Speed
		case DirLeft:
			cycle.position.X -= Speed
		case DirRight:
			cycle.position.X += Speed
		}
		if cycle.position.Y < 0 {
			cycle.position.Y = windowSize.Y
		} else if cycle.position.Y >= windowSize.Y {
			cycle.position.Y = 0
		} else if cycle.position.X < 0 {
			cycle.position.X = windowSize.X
		} else if cycle.position.X >= windowSize.X {
			cycle.position.X = 0
		}
	}
}

func (cycle *Cycle) GetPosition() Vector2f {
	return cycle.position
}

func (cycle *Cycle) SetPosition(pos Vector2f) {
	cycle.position.X = pos.X
	cycle.position.Y = pos.Y
}

func (cycle *Cycle) SetDied(died bool) {
	cycle.died = died
}

func (cycle *Cycle) AddDir(dir uint32) {
	cycle.dirQueue = append(cycle.dirQueue, dir)
}

func (cycle *Cycle) setDir(dir uint32) {
	cycle.dir = dir
}

func (cycle *Cycle) AddPosition(pos Vector2f) {
	cycle.previousPositions = append(cycle.previousPositions, pos)
}

func (cycle *Cycle) IsDead() bool {
	return cycle.died
}

func (cycle *Cycle) GetPreviousPositionsLength() int {
	return len(cycle.previousPositions)
}

func (cycle *Cycle) GetPreviousPosition(location int) Vector2f {
	return cycle.previousPositions[location]
}

func (cycle *Cycle) ResetTail() {
	cycle.previousPositions = nil
}

func (cycle *Cycle) Draw(renderer *sdl.Renderer, color f32.Vec4, headColor f32.Vec4, playerCycle bool) {
	r, g, b, a, _ := renderer.GetDrawColor()
	rect := sdl.Rect{X: int32(0), Y: int32(0), W: Size, H: Size}
	renderer.SetDrawColor(uint8(color[0]), uint8(color[1]), uint8(color[2]), uint8(color[3]))
	for i := 0; i < len(cycle.previousPositions); i++ {
		rect.X = int32(cycle.previousPositions[i].X)
		rect.Y = int32(cycle.previousPositions[i].Y)
		renderer.FillRect(&rect)
	}
	if playerCycle {
		renderer.SetDrawColor(uint8(headColor[0]), uint8(headColor[1]), uint8(headColor[2]), uint8(headColor[3]))
	}
	rect.X = int32(cycle.position.X)
	rect.Y = int32(cycle.position.Y)
	renderer.FillRect(&rect)
	renderer.SetDrawColor(r, g, b, a)
}
