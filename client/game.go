package main

import (
	"fmt"
	"strconv"

	"github.com/veandco/go-sdl2/gfx"
	"github.com/veandco/go-sdl2/sdl"
	"gitlab.com/kohlten/gamelib"
	"gitlab.com/kohlten/gamelib/vector"
)

type Game struct {
	window       *gamelib.Window
	text         *gamelib.Text
	fps          *gamelib.FpsCounter
	sceneManager *gamelib.SceneManager
	running      bool
	menuScene    *MenuScene
	gameScene    *GameScene
	client       *Client
}

func NewGame(client *Client) (*Game, bool) {
	game := new(Game)
	game.client = client
	err := game.setupWindow()
	if err != nil {
		fmt.Println(err.Error())
		return nil, false
	}
	err = game.setupScenes()
	if err != nil {
		fmt.Println(err.Error())
		return nil, false
	}
	game.running = true
	game.fps = gamelib.NewFpsCounter()
	game.text, err = gamelib.NewTextFromFile(vector.NewVec2(19, 19), "assets/numbers.png", "0123456789X", game.window.Renderer)
	if err != nil {
		fmt.Println(err.Error())
		return nil, false
	}
	game.setupClient()
	return game, true
}
func (game *Game) getEvents() {
	for {
		event := sdl.PollEvent()
		if event == nil {
			break
		}
		switch event.(type) {
		case *sdl.QuitEvent:
			game.running = false
		}
		game.sceneManager.ParseEvent(event, game.client)
	}
}

func (game *Game) update() {
	data, _ := game.sceneManager.Update(game.client)
	if game.sceneManager.GetState() == "menu" {
		if data.(int) == MenuStateQuit {
			game.running = false
		}
	} else {
		if data.(int) == GameSceneStateExit {
			game.sceneManager.Reset()
			game.sceneManager.SetCurrentScene("menu")
		}
	}
	game.fps.Update()
	game.client.Update()
}

func (game *Game) draw() {
	err := game.window.Renderer.SetDrawColor(0, 0, 0, 255)
	if err != nil {
		fmt.Println(err.Error())
		game.running = false
	}
	err = game.window.Renderer.Clear()
	if err != nil {
		fmt.Println(err.Error())
		game.running = false
	}
	err = game.sceneManager.Draw(game.window)
	if err != nil {
		fmt.Println(err.Error())
		game.running = false
	}
	showFps, _ := config.GetDataBool("showFPS")
	if showFps {
		fps := strconv.FormatUint(uint64(game.fps.GetFps()), 10)
		size := game.text.GetSize(fps, 1.0)
		pos := vector.NewVec2(float32(windowSize[0])-(size.X+5), size.Y+5)
		err = game.text.Draw(fps, pos, vector.Zero, 0.0, 1.0, nil, game.window.Renderer)
		if err != nil {
			fmt.Println(err.Error())
			game.running = false
		}
	}
	showPing, _ := config.GetDataBool("showPing")
	if showPing {
		ping := strconv.FormatUint(game.client.serverPingTime, 10)
		size := game.text.GetSize(ping, 1.0)
		pos := vector.NewVec2(float32(windowSize[0])-75, size.Y+5)
		err = game.text.Draw(ping, pos, vector.Zero, 0.0, 1.0, nil, game.window.Renderer)
		if err != nil {
			fmt.Println(err.Error())
			game.running = false
		}
	}
	game.window.Renderer.Present()
}

func (game *Game) Run() {
	fpsManager := gfx.FPSmanager{}
	gfx.InitFramerate(&fpsManager)
	gfx.SetFramerate(&fpsManager, 60)
	for game.running {
		game.getEvents()
		game.update()
		game.draw()
		gfx.FramerateDelay(&fpsManager)
	}
}

func (game *Game) Free() {
	game.window.Free()
	sendDisconnect(game.client)
	game.client.Free()
}

func (game *Game) setupClient() {
	game.client.sock.AddCallback(Ping, ping, game.client)
	game.client.sock.AddCallback(Position, setCyclePosition, game)
	game.client.sock.AddCallback(NumCycles, setNumCycles, game)
	game.client.sock.AddCallback(Identifier, setActiveCycle, game)
	game.client.sock.AddCallback(SessionStart, sessionStart, game)
	game.client.sock.AddCallback(SessionEnd, sessionEnd, game)
	game.client.sock.AddCallback(Died, died, game)
}

func (game *Game) setupWindow() error {
	settings := gamelib.NewWindowSettings()
	settings.Width = uint(windowSize[0])
	settings.Height = uint(windowSize[1])
	settings.Name = "Tron GO"
	settings.Vsync = true
	window, err := gamelib.NewWindow(settings)
	if err != nil {
		return err
	}
	game.window = window
	return nil
}

func (game *Game) setupScenes() error {
	game.sceneManager = gamelib.NewSceneManager()
	menuScene, err := NewMenuScene(game.window)
	if err != nil {
		return err
	}
	game.menuScene = menuScene
	game.sceneManager.AddScene(game.menuScene)
	game.gameScene, err = NewGameScene(game.window)
	if err != nil {
		return err
	}
	game.sceneManager.AddScene(game.gameScene)
	_ = game.sceneManager.SetCurrentScene("menu")
	return nil
}
