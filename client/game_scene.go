package main

import (
	"github.com/veandco/go-sdl2/sdl"
	"gitlab.com/kohlten/gamelib"
	"gitlab.com/kohlten/gamelib/vector"
	"golang.org/x/image/math/f32"
)

const (
	GameSceneStateNone = 0
	GameSceneStateExit = 1
)

type GameScene struct {
	pauseButtonTimer *gamelib.Timer
	text             *gamelib.Text
	sessionRunning   bool
	activeCycle      int
	state            int
	cycles           []*Cycle
}

func NewGameScene(window *gamelib.Window) (*GameScene, error) {
	gameScene := new(GameScene)
	gameScene.pauseButtonTimer = gamelib.NewTimer(10, gamelib.TimeMilliseconds)
	text, err := gamelib.NewTextFromJson("assets/fasttracker2-style_12x12.json", window.Renderer)
	if err != nil {
		return nil, err
	}
	gameScene.text = text
	gameScene.state = GameSceneStateNone
	return gameScene, nil
}

func (gameScene *GameScene) Init(data ...interface{}) {

}

func (gameScene *GameScene) Update(userData interface{}) (interface{}, error) {
	gameScene.pauseButtonTimer.Update()
	return gameScene.state, nil
}

func (gameScene *GameScene) ParseEvent(event, userData interface{}) {
	client := userData.(*Client)
	switch t := event.(type) {
	case *sdl.KeyboardEvent:
		if t.Type == sdl.KEYDOWN {
			switch t.Keysym.Sym {
			case sdl.K_s:
				sendDirection(client, gameScene.activeCycle, DirDown)
			case sdl.K_w:
				sendDirection(client, gameScene.activeCycle, DirUp)
			case sdl.K_a:
				sendDirection(client, gameScene.activeCycle, DirLeft)
			case sdl.K_d:
				sendDirection(client, gameScene.activeCycle, DirRight)
			case sdl.K_ESCAPE:
				if gameScene.cycles[gameScene.activeCycle].IsDead() || !gameScene.sessionRunning {
					gameScene.state = GameSceneStateExit
				}
			case sdl.K_p:
				if !gameScene.pauseButtonTimer.IsRunning() {
					sendPause(client)
					gameScene.pauseButtonTimer.Start()
				}

			}
		}
	}
}

func (gameScene *GameScene) Draw(window_interface interface{}) error {
	window := window_interface.(*gamelib.Window)
	for i := 0; i < len(gameScene.cycles); i++ {
		color := make([]float64, 4)
		switch i {
		case 0:
			color, _ = config.GetDataVector("snakeOneColor")
		case 1:
			color, _ = config.GetDataVector("snakeTwoColor")
		case 2:
			color, _ = config.GetDataVector("snakeThreeColor")
		case 3:
			color, _ = config.GetDataVector("snakeFourColor")
		}
		vecColor := f32.Vec4{float32(color[0]), float32(color[1]), float32(color[2]), float32(color[3])}
		headColor, _ := config.GetDataVector("snakeHeadColor")
		vecHeadColor := f32.Vec4{float32(headColor[0]), float32(headColor[1]), float32(headColor[2]), float32(headColor[3])}
		gameScene.cycles[i].Draw(window.Renderer, vecColor, vecHeadColor, i == gameScene.activeCycle)
	}
	if gameScene.cycles[gameScene.activeCycle].IsDead() || !gameScene.sessionRunning {
		if gameScene.cycles[gameScene.activeCycle].IsDead() {
			deadText := "You Died!"
			size := gameScene.text.GetSize(deadText, 1.0)
			err := gameScene.text.Draw(deadText, vector.NewVec2(float32(windowSize[0])/2-size.X/2,
				size.Y+5), vector.Zero, 0.0, 1.0,
				nil, window.Renderer)
			if err != nil {
				return err
			}
		} else {
			aliveText := "You Won!"
			size := gameScene.text.GetSize(aliveText, 1.0)
			err := gameScene.text.Draw(aliveText, vector.NewVec2(float32(windowSize[0])/2-size.X/2,
				size.Y+5), vector.Zero, 0.0, 1.0, nil, window.Renderer)
			if err != nil {
				return err
			}
		}
		text := "Press Escape to Exit"
		size := gameScene.text.GetSize(text, 1.0)
		err := gameScene.text.Draw(text, vector.NewVec2(float32(windowSize[0])/2-size.X/2,
			(size.Y+5)*2), vector.Zero, 0.0, 1.0, nil, window.Renderer)
		if err != nil {
			return err
		}
	}
	return nil
}

func (gameScene *GameScene) Reset() {
	gameScene.sessionRunning = false
	gameScene.cycles = nil
	gameScene.state = GameSceneStateNone
}

func (gameScene *GameScene) GetName() string {
	return "game"
}

func (gameScene *GameScene) Free() {
	gameScene.text.Free()
}
