package main

import (
	"fmt"
	"gitlab.com/kohlten/packet_parser"
	"strconv"
	"time"
)

const (
	PacketUnknownAction = "unknown action"
)

func ping(packet *packet_parser.PacketData, a ...interface{}) {
	client := a[0].(*Client)
	client.serverPingTime = uint64(time.Now().UnixNano() / int64(time.Millisecond)) - client.lastPing
}

func setCyclePosition(packet *packet_parser.PacketData, a ...interface{}) {
	game := a[0].(*Game)
	if len(packet.Data) < 3 {
		return
	}
	cycleId, err := strconv.Atoi(string(packet.Data[0]))
	if err != nil {
		fmt.Println("Failed to parse cycle id value!")
		return
	}
	x, err := strconv.ParseFloat(string(packet.Data[1]), 32)
	if err != nil {
		fmt.Println("Failed to parse a position value!")
		return
	}
	y, err := strconv.ParseFloat(string(packet.Data[2]), 32)
	if err != nil {
		fmt.Println("Failed to parse a position value!")
		return
	}
	previousPosition := game.gameScene.cycles[cycleId].GetPosition()
	game.gameScene.cycles[cycleId].AddPosition(previousPosition)
	game.gameScene.cycles[cycleId].SetPosition(Vector2f{x, y})
}

func setNumCycles(packet *packet_parser.PacketData, a ...interface{}) {
	game := a[0].(*Game)
	if len(packet.Data) < 1 {
		return
	}
	numCycles, err := strconv.Atoi(string(packet.Data[0]))
	if err != nil {
		fmt.Println("Failed to parse number of cycles value!")
		return
	}
	for i := 0; i < numCycles; i++ {
		game.gameScene.cycles = append(game.gameScene.cycles, NewCycle(Vector2f{X: windowSize[0], Y: windowSize[1]}, i))
	}
}

func setActiveCycle(packet *packet_parser.PacketData, a ...interface{}) {
	game := a[0].(*Game)
	if len(packet.Data) < 1 {
		return
	}
	activeCycle, err := strconv.Atoi(string(packet.Data[0]))
	if err != nil {
		fmt.Println("Failed to parse cycle id value!")
		return
	}
	game.gameScene.activeCycle = activeCycle
}

func setSessionState(game *Game, state bool) {
	game.gameScene.sessionRunning = state
}

func setCycleState(game *Game, state bool) {
	game.gameScene.cycles[game.gameScene.activeCycle].SetDied(state)
}

func sessionStart(packet *packet_parser.PacketData, a ...interface{}) {
	game := a[0].(*Game)
	game.sceneManager.Reset()
	_ = game.sceneManager.SetCurrentScene("game")
	setSessionState(game, true)
	setCycleState(game, false)
}

func sessionEnd(packet *packet_parser.PacketData, a ...interface{}) {
	game := a[0].(*Game)
	setSessionState(game, false)
}

func died(packet *packet_parser.PacketData, a ...interface{}) {
	game := a[0].(*Game)
	setCycleState(game, true)
}
