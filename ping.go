package main

import (
	"fmt"
	"gitlab.com/kohlten/game_server"
	gameClient "gitlab.com/kohlten/game_server/client"
	"gitlab.com/kohlten/packet_parser"
	"os"
	"strconv"
)

const (
	Ping = 0x71
	Disconnect = 0x72
)

const maxWaitTime = 1000

func main() {
	if len(os.Args) != 4 {
		fmt.Println("Usage: ./ping <ADDRESS> <PORT> <TIMES>")
		os.Exit(1)
	}
	port, err := strconv.ParseUint(os.Args[2], 10, 16)
	if err != nil {
		panic(err)
	}
	runTimes, err := strconv.ParseUint(os.Args[3], 10, 64)
	if err != nil {
		panic(err)
	}
	client, err := gameClient.NewGameClientCallbacks(os.Args[1], uint16(port), 16, packet_parser.FormatPacket, packet_parser.ParsePacket)
	if err != nil {
		panic(err)
	}
	responded := false
	pingFunc := func (data *packet_parser.PacketData, a ...interface{}) {
		responded = true
	}
	client.AddCallback(Ping, pingFunc)
	times := make([]uint64, runTimes)
	packet := packet_parser.FormatPacket(Ping)
	for i := 0; i < int(runTimes); i++ {
		client.SendPacket(packet)
		start := game_server.GetTime()
		for {
			if game_server.GetTime() - start > maxWaitTime {
				fmt.Println("Server at ", os.Args[1], ":", os.Args[2], " Is not pinging back")
				os.Exit(1)
			}
			if responded {
				break
			}
		}
		responded = false
		respondTime := game_server.GetTime() - start
		fmt.Println("Took ", respondTime, " for the server to respond ")
		times[i] = respondTime
	}
	packet = packet_parser.FormatPacket(Disconnect)
	client.SendPacket(packet)
	client.Close()
	average := uint64(0)
	for i := 0; i < len(times); i++ {
		average += times[i]
	}
	average /= runTimes
	fmt.Println("Average ping: ", average)
}
