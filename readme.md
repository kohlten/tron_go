<h1> Tron GO </h1>

Tron GO is a simple multiplayer game where you have cycles with tails. These tails grow infinitly as the cycles move. When another cycle hits your tail, they will die. The goal is to be the last cycle standing. 

Compliling:
```
	To continue you need a working version of golang

	go get github.com/veandco/go-sdl2/sdl
	go get golang.org/x/image/math/f32
	go get gitlab.com/kohlten/packet_parser
	go get gitlab.com/kohlten/game_server/client
	go get gitlab.com/kohlten/gamelib

	cd client && go build
	cd ../server && go build
```

![image one](/images/1.png)
![image two](/images/2.png)
![image three](/images/3.png)
